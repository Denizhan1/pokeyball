﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stick : MonoBehaviour
{
    public GameObject ball;
    private void OnTriggerEnter(Collider coll)
    {
        ball.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);

        if (coll.gameObject.tag == "Platform")
        {
            ball.GetComponent<BallControl>().collision = true;
            ball.GetComponent<Animator>().ResetTrigger("Coll");
            ball.GetComponent<Rigidbody>().useGravity = false;
        }
        if (coll.gameObject.tag == "Obstacle")
        {
            ball.GetComponent<Animator>().SetTrigger("Coll");
        }
    }
}
