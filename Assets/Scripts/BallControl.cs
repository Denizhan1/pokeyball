﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallControl : MonoBehaviour
{
    [SerializeField] private LayerMask targetLayer;
    [SerializeField] private float rotationRate = 0.02f;

    public bool flight = false;
    public TMPro.TextMeshProUGUI Height;
    public Transform ball;

    private float m_previousY;
    private float m_previousX;
    public float toplamDelta;
    public Animator animator;
    public bool collision = false;
    private float delta;

    void Start()
    {
        animator.GetComponent<Animator>();
        animator.speed = 0;
    }

    private void FixedUpdate()
    {
        Height.text = "Height: " + Convert.ToInt32(transform.position.y).ToString() + " m";
        if (transform.position.y < 32)
        {
            FindObjectOfType<Manager>().Restart();
        }

#if !MOBILE_INPUT
        Control_PC();
#else
            Control_Mobile();
#endif
    }
    private void Control_PC()
    {
        if (!flight)
        {
            if (Input.GetMouseButtonDown(0))
            {
                animator.speed = 0;
                animator.SetTrigger("Bend");
                toplamDelta = 0.0f;
                delta = 0;
                m_previousY = Input.mousePosition.y;
            }
            if (Input.GetMouseButton(0) && m_previousY > Input.mousePosition.y)
            {
                delta = (m_previousY - Input.mousePosition.y);
                m_previousY = Input.mousePosition.y;
                animator.speed = delta / 1000;
                toplamDelta += delta;
                if (toplamDelta >= 300)
                    toplamDelta = 300;
            }
            if (Input.GetMouseButtonUp(0))
            {
                animator.ResetTrigger("Bend");
                animator.speed = toplamDelta / 60;
                animator.SetTrigger("Release");
                animator.speed = 1;
                Invoke("Force", 0.08f);
                delta = 0;
            }
        }
        if (flight)
        {
            if (Input.GetMouseButtonDown(0))
            {
                animator.SetTrigger("Tap");
            }
        }

        if (collision)
        {
            if (GetComponent<Rigidbody>().velocity.y > 0)
            {
                animator.SetTrigger("OscUp");
            }
            else
            {
                animator.SetTrigger("OscDown");
            }
            GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
            flight = false;
            collision = false;
        }
    }
    public void Force()
    {
        flight = true;
        ball.GetComponent<Rigidbody>().useGravity = true;
        ball.GetComponent<Rigidbody>().AddForce(ball.up * toplamDelta * 20);
    }
}
